#include "main-transform.h"
#include <stdio.h>

int main(void){
    printf("%lu\n", sizeof(float));
    char* arg[] = {"", "/home/sterus/CLionProjects/assignment-3-image-rotation/solution/sepia/temp/input.bmp", "/home/sterus/CLionProjects/assignment-3-image-rotation/solution/sepia/temp/output_asm.bmp", "/home/sterus/CLionProjects/assignment-3-image-rotation/solution/sepia/temp/output_C.bmp", "-90"};
    int result = main_transform(4, arg);
    if(result == -1){
        printf("Something bad happened\n");
        return -1;
    }
    printf("Successful\n");
    return 0;
}
