#ifndef BMPTEST_IMAGE_READ_H
#define BMPTEST_IMAGE_READ_H
#include <inttypes.h>
#ifndef STRUCTURES_H
#include "structures.h"
#endif

enum read_status  {
    READ_OK = 0,
    READ_ERROR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};



#endif //BMPTEST_IMAGE_READ_H
