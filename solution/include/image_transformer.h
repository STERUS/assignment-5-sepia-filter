#ifndef BMPTEST_IMAGE_TRANSFORMER_H
#define BMPTEST_IMAGE_TRANSFORMER_H
#ifndef STRUCTURES_H

#include "structures.h"
#include <stdbool.h>
#endif
struct image rotate(struct image, int16_t);
struct image sepia_filter(struct image, bool useAsm);
struct image same(struct image src);
#endif //BMPTEST_IMAGE_TRANSFORMER_H
