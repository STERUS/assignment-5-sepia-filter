
enable_language(ASM_NASM)

set(CMAKE_ASM_NASM_OBJECT_FORMAT elf64)
set(CMAKE_C_STANDARD 11)
set(CMAKE_ASM_NASM_COMPILE_OBJECT "<CMAKE_ASM_NASM_COMPILER> <INCLUDES> \
    <FLAGS> -f ${CMAKE_ASM_NASM_OBJECT_FORMAT} -o <OBJECT> <SOURCE>")


file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    src/*.asm
    include/*.h
)

add_executable(image-transformer ${sources}
        src/sepia.asm
        sepia/main.c)
target_include_directories(image-transformer PRIVATE src include sepia sepia/temp)
