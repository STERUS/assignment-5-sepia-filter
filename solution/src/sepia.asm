%macro char_to_float 1
    pmovzxbd %1, %1
    cvtdq2ps %1, %1
%endmacro
%macro set_xmm_matrix 3
    movdqu xmm3, [rel %1]
    movdqu xmm4, [rel %2]
    movdqu xmm5, [rel %3]
%endmacro
%macro muls_matrix 0
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2

    cvtps2dq xmm0, xmm0
    pminud xmm0, xmm6
%endmacro
%macro extract_from_xmm 1
    pextrb [rdi + %1], xmm0, 0
    pextrb [rdi + %1 + 1], xmm0, 4
    pextrb [rdi + %1 + 2], xmm0, 8
    pextrb [rdi + %1 + 3], xmm0, 12

%endmacro
section .data
b0: dd 0.131, 0.168, 0.189, 0.131
g0: dd 0.534, 0.686, 0.769, 0.534
r0: dd 0.272, 0.349, 0.393, 0.272

b1: dd 0.168, 0.189, 0.131, 0.168
g1: dd 0.686, 0.769, 0.534, 0.686
r1: dd 0.349, 0.393, 0.272, 0.349

b2: dd 0.189, 0.131, 0.168, 0.189
g2: dd 0.769, 0.534, 0.686, 0.769
r2: dd 0.393, 0.272, 0.349, 0.393

max_values_vec: dd 255, 255, 255, 255

section .text

global sepia_asm

sepia_asm:
    ;получаем адреса на data и сохраняем их (uint64_t весит 8 байт)
    push r12
    push r13
    mov r12, [rdi + 16]
    mov r13, [rsi + 16]
    movups xmm6, [rel max_values_vec]
    ;Размеры изображения
    mov r8, [rdi]
    mov r9, [rdi + 8]
    imul r8, r9
    ;Будем обрабатывать по 4 пикселя и оставшиеся обработаем вручную
    sub r8, 4
    push r8
    push 0
    .cycle1_start:
        mov rdi, [rsp]
        cmp rdi, [rsp + 8]
        ja .cycle1_end
        ;итерация цикла
        imul rdi, 3
        mov rsi, rdi
        add rsi, r12
        add rdi, r13

        ;теперь в rdi начало блока из 4 пикселей, которые мы хотим обработать, в rsi - тот же блок исходного изображения
        mov al, [rsi + 3]
        pinsrb xmm0, al, 3
        mov al, [rsi]
        pinsrb xmm0, al, 0
        pinsrb xmm0, al, 1
        pinsrb xmm0, al, 2

        char_to_float xmm0

        mov al, [rsi + 4]
        pinsrb xmm1, al, 3
        mov al, [rsi + 1]
        pinsrb xmm1, al, 0
        pinsrb xmm1, al, 1
        pinsrb xmm1, al, 2

        char_to_float xmm1

        mov al, [rsi + 5]
        pinsrb xmm2, al, 3
        mov al, [rsi + 2]
        pinsrb xmm2, al, 0
        pinsrb xmm2, al, 1
        pinsrb xmm2, al, 2

        char_to_float xmm2
        set_xmm_matrix b0, g0, r0
        muls_matrix
        extract_from_xmm 0

        ;треть готова
        mov al, [rsi + 3]
        pinsrb xmm0, al, 1
        pinsrb xmm0, al, 0
        mov al, [rsi + 6]
        pinsrb xmm0, al, 3
        pinsrb xmm0, al, 2

        char_to_float xmm0

        mov al, [rsi + 4]
        pinsrb xmm1, al, 1
        pinsrb xmm1, al, 0
        mov al, [rsi + 7]
        pinsrb xmm1, al, 3
        pinsrb xmm1, al, 2

        char_to_float xmm1

        mov al, [rsi + 5]
        pinsrb xmm2, al, 1
        pinsrb xmm2, al, 0
        mov al, [rsi + 8]
        pinsrb xmm2, al, 3
        pinsrb xmm2, al, 2

        char_to_float xmm2
        set_xmm_matrix b1, g1, r1
        muls_matrix
        extract_from_xmm 4

        ;две трети готово
        mov al, [rsi + 6]
        pinsrb xmm0, al, 0
        mov al, [rsi + 9]
        pinsrb xmm0, al, 3
        pinsrb xmm0, al, 1
        pinsrb xmm0, al, 2

        char_to_float xmm0

        mov al, [rsi + 7]
        pinsrb xmm1, al, 0
        mov al, [rsi + 10]
        pinsrb xmm1, al, 3
        pinsrb xmm1, al, 1
        pinsrb xmm1, al, 2

        char_to_float xmm1

        mov al, [rsi + 8]
        pinsrb xmm2, al, 0
        mov al, [rsi + 11]
        pinsrb xmm2, al, 3
        pinsrb xmm2, al, 1
        pinsrb xmm2, al, 2

        char_to_float xmm2
        set_xmm_matrix b2, g2, r2
        muls_matrix
        extract_from_xmm 8

        mov rdi, [rsp]
        ;обработали 4 пикселя
        add rdi, 4
        mov [rsp], rdi
        jmp .cycle1_start
    .cycle1_end:
        add rsp, 16
        pop r13
        pop r12
        ret
