#include "file_accesser.h"
#include <stdio.h>

enum open_status open_file_for_read(const char* file_name, FILE** input){
    FILE* result = fopen(file_name, "rb");
    if(result == NULL){
        return OPEN_NULL;
    }
    *input = result;
    return OPEN_OK;
}

enum open_status open_file_for_write(const char* file_name, FILE** output){
    FILE* result = fopen(file_name, "wb");
    if(result == NULL){
        return OPEN_NULL;
    }
    *output = result;
    return OPEN_OK;
}

enum close_status close_file(FILE** input){
    if (input == NULL){
        return CLOSE_ERROR;
    }
    int result = fclose(*input);
    if(result == 0){
        return CLOSE_OK;
    }
    return CLOSE_ERROR;
}
