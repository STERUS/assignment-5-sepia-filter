#include "structures.h"
#include <malloc.h>
#include <stdbool.h>
#include <time.h>
void sepia_asm(struct image* src, struct image* new);
//Создадим макрос, чтобы проверять результат malloc на NULL
#define alloc(ptr, size)\
\
    ptr = malloc(size);    \
    if((ptr) == NULL){      \
        return (struct image){0};                    \
    }



static void copy_data(struct image src, struct image new){
    for(size_t i = 0; i < src.width * src.height; i++){
        new.data[i] = src.data[i];

    }
}

struct image same(const struct image src){
    struct image new_image = { .width = src.width, .height = src.height, .data = NULL};
    alloc( new_image.data, (sizeof(struct pixel) * new_image.width * new_image.height));
    copy_data(src, new_image);
    return new_image;
}

static struct image rotate270(const struct image src){
    struct image new_image = { .width = src.height, .height = src.width, .data = NULL};
    alloc( new_image.data, sizeof(struct pixel) * new_image.width * new_image.height);

    size_t counter = 0;
    for(size_t i = 0; i < src.width; i++){
        size_t temp = i + (src.height - 1) * src.width;
        for(size_t j = 0; j < src.height; j++){
            new_image.data[counter] = src.data[temp];
            temp -= src.width;
            counter++;
        }
    }
    return new_image;
}

static struct image rotate90(const struct image src){
    struct image new_image = { .width = src.height, .height = src.width, .data = NULL};
    alloc( new_image.data, sizeof(struct pixel) * new_image.width * new_image.height);

    size_t counter = 0;
    for(size_t i = src.width; i > 0; i--){
        size_t temp = i - 1;
        for(size_t j = 0; j < src.height; j++){
            new_image.data[counter] = src.data[temp];
            temp += src.width;
            counter++;
        }
    }
    return new_image;
}

static struct image rotate180(const struct image src){
    struct image new_image = { .width = src.width, .height = src.height, .data = NULL};
    alloc( new_image.data, sizeof(struct pixel) * new_image.width * new_image.height);

    size_t counter = 0;
    for(size_t i = src.height; i > 0; i--){
        size_t temp = i * src.width - 1;
        for(size_t j = src.width; j > 0; j--){
            new_image.data[counter] = src.data[temp];
            temp -= 1;
            counter++;
        }
    }
    return new_image;
}

struct image rotate(const struct image src, int16_t angle){
    if(angle == 0){
        return same(src);
    }
    if(angle == -90 || angle == 270){
        return rotate270(src);
    }
    if(angle == 90 || angle == -270){
        return rotate90(src);
    }
    if (angle == 180 || angle == -180){
        return rotate180(src);
    }

    return same(src);
}

static void sepia_one(struct pixel* pixel, float r, float g, float b){
    pixel->r = (r * 0.393f + g * 0.769f + b * 0.189f) > 255 ? 255 : (uint8_t)(r * 0.393f + g * 0.769f + b * 0.189f);
    pixel->g = (r * 0.349f + g * 0.686f + b * 0.168f) > 255 ? 255 : (uint8_t)(r * 0.349f + g * 0.686f + b * 0.168f);
    pixel->b = (r * 0.272f + g * 0.534f + b * 0.131f) > 255 ? 255 : (uint8_t)(r * 0.272f + g * 0.534f + b * 0.131f);
}

static void sepia_algorithm(const struct image src, struct image new){
    for (size_t i = 0; i < src.width * src.height; ++i) {
        float new_r = src.data[i].r;
        float new_g = src.data[i].g;
        float new_b = src.data[i].b;

        sepia_one(&(new.data[i]), new_r, new_g, new_b);
    }
}

struct image sepia_filter(struct image src, bool use_asm){
    struct image new_image = { .width = src.width, .height = src.height, .data = NULL};
    alloc( new_image.data, sizeof(struct pixel) * new_image.width * new_image.height);
    printf("\n");

    if(use_asm) {
        //С использованием simd инструкций
        clock_t now = clock();

        sepia_asm(&src, &new_image);

        for (size_t i = 0; i < (src.width * src.height) % 4; i++) {
            sepia_one(&(new_image.data[src.width * src.height - i]), src.data[i].r, src.data[i].g, src.data[i].b);
        }
        double first = (double) (clock() - now) / CLOCKS_PER_SEC;
        printf("\nНа выполнения с использованием SIMD ушло: %f секунд\n", first);
    } else {
        clock_t now = clock();
        sepia_algorithm(src, new_image);
        double second = (double)(clock() - now) / CLOCKS_PER_SEC;

        printf("\nНа выполнения без SIMD ушло: %f секунд\n\n", second);
    }
    //Без использования simd инструкций
    return new_image;
}
