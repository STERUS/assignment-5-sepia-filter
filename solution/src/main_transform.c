#include "bmp_reader.h"
#include "file_accesser.h"
#include "image_transformer.h"
#include "main-transform.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main_transform( int argc, char** argv ) {
    if (argc != 4){
        fprintf(stderr, "Expected 4 parameters: <source-image> <transformed-image> <angle>");
        return -1;
    }
    long angle = strtol(*(argv + 4), (char **) '\0', 10);
    angle = angle % 360;
    if(angle % 90 != 0){
        fprintf(stderr, "Invalid angle");
        return -1;
    }
    //printf("%"PRId16"\n", angle);
    fprintf(stdout,"Opening file: %s\n", *(argv + 1));
    FILE* input;
    enum open_status on_open = open_file_for_read(*(argv + 1), &input);
    if (on_open == OPEN_NULL){
        fprintf(stderr, "Failed to open file: %s\n", *(argv + 1));
        return -1;
    }
    //Получили объект FILE

    //Создаем и инициализируем объект картинки
    struct image our_image = {0};
    void* header; //Вот тут есть пустой указатель, когда мы создадим header файла внутри from_bmp, как по мне, нет смысла переписывать header заново, я сохраню его, а потом просто изменю его поля если потребуется
    enum read_status on_read = from_bmp(input, &our_image, &header);
    if (on_read != READ_OK){
        //Если что-то пошло не так, то просто освобождаем память и выходим
        free_memory(&our_image);
        free_header(&header);
        fprintf(stderr, "Error While init image\n");
        return -1;
    }

    //Закрываем уже ненужный файл
    fprintf(stdout,"Closing file: %s\n", *(argv + 1));
    enum close_status on_close = close_file(&input);
    if (on_close == CLOSE_ERROR){
        fprintf(stderr, "Failed to close file: %s\n", *(argv + 1));
    }

    //Отладочный принт, показывающий количество пикселей и 2 первых пикселя
    //printf("%"PRIu64"\n %"PRIu64"\n %"PRIu8" %"PRIu8" %"PRIu8",  %"PRIu8" %"PRIu8" %"PRIu8"\n"  , our_image.width, our_image.height, our_image.data[0].r, our_image.data[0].g, our_image.data[0].b, our_image.data[1].r, our_image.data[1].g, our_image.data[1].b);

    //Трансформируем Картинку
    struct image transformed_image_C = sepia_filter(our_image, false);
    struct image transformed_image_asm = sepia_filter(our_image, true);

    /*free_memory(&our_image);*/
    //Если освободить не получилось, то освобождаем память и выходим
    if(transformed_image_asm.data == NULL){
        free_header(&header);
        return -1;
    }
    if(transformed_image_C.data == NULL){
        free_header(&header);
        return -1;
    }

    //Открываем новый файл
    FILE* output_asm;
    FILE* output_C;
    enum open_status on_open_new_file_asm = open_file_for_write(*(argv + 2) , &output_asm);
    enum open_status on_open_new_file_C = open_file_for_write(*(argv + 3), &output_C);
    //Если открыть файл для записи не получилось, то тоже освобождаем память и выходим
    if (on_open_new_file_asm == OPEN_NULL || on_open_new_file_C == OPEN_NULL){
        fprintf(stderr, "Failed to open file: %s\n", *(argv + 2));
        free_memory(&transformed_image_asm);
        free_memory(&transformed_image_C);
        free_header(&header);
        return -1;
    }

    //Записали файл
    enum write_status on_to_bmp_status_C = to_bmp(output_C, &transformed_image_C, header);
    enum write_status on_to_bmp_status_asm = to_bmp(output_asm, &transformed_image_asm, header);

    if(on_to_bmp_status_C == WRITE_ERROR || on_to_bmp_status_asm == WRITE_ERROR){
        close_file(&output_C);
        close_file(&output_asm);
        free_memory(&transformed_image_asm);
        free_memory(&transformed_image_C);
        free_header(&header);
        return -1;
    }
    //Закрываем новый файл
    fprintf(stdout,"Closing file: %s\n", *(argv + 2));
    on_close = close_file(&output_asm);
    if (on_close == CLOSE_ERROR){
        fprintf(stderr, "Failed to close file: %s\n", *(argv + 2));
    }
    fprintf(stdout,"Closing file: %s\n", *(argv + 3));
    on_close = close_file(&output_C);
    if (on_close == CLOSE_ERROR){
        fprintf(stderr, "Failed to close file: %s\n", *(argv + 3));
    }
    free_memory(&transformed_image_asm);
    free_memory(&transformed_image_C);
    free_header(&header);
    return 0;
}
